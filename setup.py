from setuptools import setup

setup(
    name='biblr',
    packages=['biblr'],
    include_package_data=True,
    install_requires=[
        'flask',
        'requests',
    ],
)
